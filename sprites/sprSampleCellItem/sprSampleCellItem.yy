{
    "id": "31f035d2-a71c-4a46-be69-d1e41a6760a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSampleCellItem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 17,
    "bbox_right": 42,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7904ba34-7e8e-4a4d-a3da-9b839445b7fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31f035d2-a71c-4a46-be69-d1e41a6760a4",
            "compositeImage": {
                "id": "81908fbe-b75f-4913-9254-89d5a896cf2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7904ba34-7e8e-4a4d-a3da-9b839445b7fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9d82160-c037-4ae1-92ef-4848863285d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7904ba34-7e8e-4a4d-a3da-9b839445b7fd",
                    "LayerId": "3f63aa80-ebc2-4285-b23b-73d5368a239e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3f63aa80-ebc2-4285-b23b-73d5368a239e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31f035d2-a71c-4a46-be69-d1e41a6760a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}