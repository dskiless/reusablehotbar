{
    "id": "6506d28c-74aa-4a5f-b235-7c082fd80496",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSampleActiveHotbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffa56f19-367b-40ba-8f9e-5c6f951e5401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6506d28c-74aa-4a5f-b235-7c082fd80496",
            "compositeImage": {
                "id": "756e5c5b-672c-4c4f-a9e1-c7b72ee3351a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa56f19-367b-40ba-8f9e-5c6f951e5401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5d923ba-1f1b-447e-8936-8fee4572e6c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa56f19-367b-40ba-8f9e-5c6f951e5401",
                    "LayerId": "71cacf7f-6585-44e4-ab48-dcee72b9ce3c"
                }
            ]
        },
        {
            "id": "767fcd65-f210-4bac-8d52-dd8318d1a6b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6506d28c-74aa-4a5f-b235-7c082fd80496",
            "compositeImage": {
                "id": "372a5def-699b-4fc0-b107-c83e6d2ca693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "767fcd65-f210-4bac-8d52-dd8318d1a6b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ab229b-a515-4f67-aaf6-11e85c9e86cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "767fcd65-f210-4bac-8d52-dd8318d1a6b8",
                    "LayerId": "71cacf7f-6585-44e4-ab48-dcee72b9ce3c"
                }
            ]
        },
        {
            "id": "2dc465fc-90d6-45e5-b148-ba87c8fddd95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6506d28c-74aa-4a5f-b235-7c082fd80496",
            "compositeImage": {
                "id": "11f0c380-c42b-45cf-a916-9b3487f62b37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dc465fc-90d6-45e5-b148-ba87c8fddd95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7698e3ac-4fcc-451a-afd6-587eeb15d3e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dc465fc-90d6-45e5-b148-ba87c8fddd95",
                    "LayerId": "71cacf7f-6585-44e4-ab48-dcee72b9ce3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "71cacf7f-6585-44e4-ab48-dcee72b9ce3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6506d28c-74aa-4a5f-b235-7c082fd80496",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}