{
    "id": "b759d688-cea3-4a03-a846-082291156b8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSampleHotbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f479ce1-5df2-4750-91ee-e1550d377c2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b759d688-cea3-4a03-a846-082291156b8c",
            "compositeImage": {
                "id": "c5ae1c48-419e-4132-b651-60b35950dcab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f479ce1-5df2-4750-91ee-e1550d377c2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220b677e-ef4e-4498-a353-39be15fa325f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f479ce1-5df2-4750-91ee-e1550d377c2a",
                    "LayerId": "2e3fa799-1335-449b-8d11-819fa5632d32"
                }
            ]
        },
        {
            "id": "de6fb10c-f52c-4890-9925-8728b0fe40a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b759d688-cea3-4a03-a846-082291156b8c",
            "compositeImage": {
                "id": "db645da9-5282-402e-917d-b3db21adbd99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de6fb10c-f52c-4890-9925-8728b0fe40a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c05ee823-a585-4540-b451-bf6eeee87d61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de6fb10c-f52c-4890-9925-8728b0fe40a9",
                    "LayerId": "2e3fa799-1335-449b-8d11-819fa5632d32"
                }
            ]
        },
        {
            "id": "7dd7afc6-21c1-4058-97e3-0ab4e31c6a20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b759d688-cea3-4a03-a846-082291156b8c",
            "compositeImage": {
                "id": "63ab4f18-aa1a-4a1a-a9ce-18eb1cda1ac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd7afc6-21c1-4058-97e3-0ab4e31c6a20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7870909-70ed-41d8-b90f-fe82b9bfb0ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd7afc6-21c1-4058-97e3-0ab4e31c6a20",
                    "LayerId": "2e3fa799-1335-449b-8d11-819fa5632d32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2e3fa799-1335-449b-8d11-819fa5632d32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b759d688-cea3-4a03-a846-082291156b8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}