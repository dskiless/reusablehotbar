////SetBarElement(cellIndex, cellSprite);

var cellIndex = argument[0];
var cellSprite = argument[1];

with(objHotbar) {
	if(cellIndex > 0 && cellIndex < cells) {
		cellElements[cellIndex] = cellSprite;
	}
}