////GetBarCell(cellIndex, baseSprite, activeSprite, currentCell);

var cellIndex = argument[0];
var baseSprite = argument[1];
var activeSprite = argument[2];
var currentCell = argument[3];

if(currentCell == cellIndex) {
	return 	activeSprite;
} else {
	return baseSprite;
}