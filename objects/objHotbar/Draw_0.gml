#macro LEFT 0
#macro CENTER 1
#macro RIGHT 2

var baseSprite = asset_get_index(spriteName);
var activeSprite = asset_get_index(activeSpriteName);

//Subtrace one to force overlap of edge.
var spriteWidth = sprite_get_width(baseSprite);

var centerElements = cells - 2;

//Draw leftmost
var currentSprite = scrGetBarCell(0, baseSprite, activeSprite, currentCell);
draw_sprite(currentSprite, LEFT, x, y);

for(var i = 1; i <= centerElements; i++) {
	currentSprite = scrGetBarCell(i, baseSprite, activeSprite, currentCell);
	draw_sprite(currentSprite, CENTER, x + (spriteWidth * i), y);	
}

//Draw rightmost.
currentSprite = scrGetBarCell((cells - 1), baseSprite, activeSprite, currentCell);
draw_sprite(currentSprite, RIGHT, x + (spriteWidth * (cells - 1)), y);

for(var i = 0; i < cells; i++) {
	if(noone != cellElements[i]) {
		draw_sprite(cellElements[i], 0, x + (spriteWidth * i), y);
	}
}